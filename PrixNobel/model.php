<?php 
    // This class represents a Model object in an MVC architecture
    class Model {
        // Private variable to hold the database connection
        private $connection;
    
        // Constructor function to create a new Model object
        public function __construct() {
            try {
                // Establish a new PDO connection to the database
                $this->connection = new PDO('mysql:host=localhost;dbname=nobelstest', 'root', '');
            } catch (PDOException $e) {
                // If an exception is thrown, throw the exception again
                throw $e;
            }
        }
        
    
        // Function to retrieve the 25 most recent Nobel Prizes from the database
        public function get_last() {
            // Define the SQL query to select all fields from the 'nobels' table, ordered by year in descending order, and limited to 25 rows
            $sql = "SELECT * FROM nobels ORDER BY year DESC LIMIT 25";
    
            // Prepare the statement
            $stmt = $this->connection->prepare($sql);
    
            // Execute the statement
            $stmt->execute();
    
            // Return the statement
            return $stmt;
        }
    
        // Function to count the number of rows in the 'nobels' table
        public function get_nb_nobel_prizes() {
            // Define the SQL query to count the number of rows in the 'nobels' table
            $sql = "SELECT count(*) FROM nobels";
    
            // Execute the query and store the result
            $result = $this->connection->query($sql);
    
            // Return the number of rows from the result using the fetchColumn() method
            return $result->fetchColumn();
        }
    }
    
    ?>
    

